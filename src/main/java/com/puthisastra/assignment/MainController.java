package com.puthisastra.assignment;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/books")
public class MainController {

	@Autowired
	private BookRepository bookRepository;
	
	@GetMapping("/create")
	@ResponseBody
	public Book createBook() {
		Book book = new Book();
		book.setTitle("something");
		book.setPages(50);
		
		Book book2 = new Book();
		book2.setTitle("test title");
		book2.setPages(100);
		
		Book book3 = new Book();
		book3.setTitle("new title");
		book3.setPages(1001);
		
		bookRepository.save(book2);
		bookRepository.save(book3);
	
		return bookRepository.save(book);
	}
	
	//display all book in db
	@GetMapping
	@ResponseBody
	public List<Book> getAllBooks() {
		return bookRepository.findAll();
	}
	
	@GetMapping("/getBookById")
	@ResponseBody
	public Optional<Book> getBookById() {
		return bookRepository.findById(1l);
	}
	
	@GetMapping("update")
	@ResponseBody
	public List<Book> updateBookById() {
		bookRepository.setTitle("new value", 1l);
		return bookRepository.findAll();
		
	}
	
	@GetMapping("delete")
	@ResponseBody
	public void deleteBookById() {
		bookRepository.deleteById(1L);
	}
	
	@GetMapping("/findByPagesGreaterThan")
	@ResponseBody
	public List<Book> getBookByPageGt(){
		return bookRepository.findByPagesGreaterThan(100);
	}
	
	//find book where title = something
	@GetMapping("/getBookByTitle")
	@ResponseBody
	public List<Book> getBookByTitle(){
		return bookRepository.findByTitle("something");
	}
	
	//find book where title = something and page = 50
	@GetMapping("/getBookByTitleAndPage")
	@ResponseBody
	public List<Book> getBookByTitleAndPage(){
		return bookRepository.findByTitleAndPages("something",50);
	}
	
	//find book where page between 50 and 100
	@GetMapping("/getBookByTitleBetweenPage")
	@ResponseBody
	public List<Book> getBookByTitleBetweenPage(){
		return bookRepository.findByPagesBetween(50, 100);
	}
	
	//find max page custom query
	@GetMapping("/findByPageQueryMax")
	@ResponseBody
	public Integer findByPageQueryMax() {
		return bookRepository.findByPageQueryMax();
	}
	
	//Get books sort by title descending
	@GetMapping("/findByTitleQuerySort")
	@ResponseBody
	public List<Book> findByTitleQuerySort() {
		return bookRepository.findByTitleQuerySort();
	}
	
	//Get books where title = ‘something’ or pages is equals 50 (passed in parameters)
	@GetMapping("/findByTitleQueryOr")
	@ResponseBody
	public List<Book> findByTitleQueryOr() {
		return bookRepository.findByTitleQueryOr("something",50);
	}
}
