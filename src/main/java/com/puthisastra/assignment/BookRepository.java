package com.puthisastra.assignment;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

public interface BookRepository extends JpaRepository<Book, Long>{

	Optional<Book> findById(Long id);
	
	@Modifying
	@Transactional
	@Query("update Book b set b.title = ?1 where b.id = ?2")
	int setTitle(String title, Long id);
	
	List<Book> findByPagesGreaterThan(Integer pages);
	
	List<Book> findByTitle(String title);
	
	List<Book> findByTitleAndPages(String title, Integer pages);
	
	List<Book> findByPagesBetween(Integer page1, Integer page2);
	
	@Query("select MAX(b.pages) from Book b")
	Integer findByPageQueryMax();
	
	//Get books sort by title descending
	@Query("select b from Book b order by b.title desc")
	List<Book> findByTitleQuerySort();
	
	//Get books where title = ‘something’ or pages is equals 50 (passed in parameters)
	@Query("select b from Book b where b.title = ?1 OR b.pages = ?2")
	List<Book> findByTitleQueryOr(String title, Integer pages);
	
}
